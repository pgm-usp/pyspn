folder='../learned-spns' #benchmark_heitor'
files=(
	'liver-disorders' 
	'spambase' 
	#'diabetes'
	'heart-statlog' 
	#'nltcs'
	#'vote' 
	#'hepatitis'
	#'ionosphere' 
	#"sonar" 
	# 'mushrooms'
     )

for filename in ${files[*]} 
do
   echo $folder/$filename
   python binarizer.py --spn_file $folder/$filename/$filename.spn
   python spn2milp.py --spn_file $folder/$filename/$filename.spn2l
   i=1
   head -3 $folder/$filename/$filename.evid | 
   while read a; do
     echo $a > $folder/$filename/$filename$i.evid
     ((i=i+1))
   done
   i=1
   head -3 $folder/$filename/$filename.query | 
   while read a; do
     echo $a > $folder/$filename/$filename$i.query
     ((i=i+1))
   done
   for j in 1 2 3
   do
   python milp2map.py --lp_file $folder/$filename/$filename.lp --query_file $folder/$filename/$filename$j.query --evid_file $folder/$filename/$filename$j.evid --spn_file $folder/$filename/$filename.spn2l --output_file $folder/$filename/$filename.result$j --timeout 600 --multiplier 1.0 --show_map_states 1 
#
   done
done
