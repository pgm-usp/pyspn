from typing import Tuple, cast, List, Optional
import math
from spn.actions.map_algorithms.map_scenario import MapScenario
from spn.node.base import SPN
from spn.utils.evidence import Evidence
from spn.node.indicator import Indicator
from spn.structs import Variable


def do_argmax_product(map_scenario: MapScenario) -> Evidence:
    """Caller for argmax_product to adequate returned value to be only
    the Evidence"""
    return argmax_product(
        map_scenario.spn,
        map_scenario.query,
        map_scenario.evidence,
        map_scenario.marginalized,
    )[0]


def argmax_product(
    spn: SPN,
    query: List[Variable],
    evidence: Optional[Evidence],
    marginalized: Optional[List[Variable]],
) -> Tuple[Evidence, float]:
    """Argmax-product algorithm based on 'Approximation Complexity of Maximum A
    Posteriori Inference in Sum-Product Networks'"""
    if spn.type == "sum":
        results = []
        for child in spn.children:
            ev = argmax_product(child, query, evidence, marginalized)[0]
            results.append((ev, spn.log_value(ev)))
        return max(results, key=lambda x: x[1])
    if spn.type == "product":
        ev = Evidence()
        child_evidences = [
            argmax_product(child, query, evidence, marginalized)[0]
            for child in spn.children
        ]
        for child_evidence in child_evidences:
            ev.merge(child_evidence)
        return (ev, spn.log_value(ev))
    # Leaf (indicator)
    spn = cast(Indicator, spn)
    variable = spn.variable
    if evidence is not None and variable in evidence.variables:
        ev = Evidence({variable: evidence[variable]})
        return ev, spn.log_value(ev)
    if marginalized is not None and variable in marginalized:
        return Evidence({variable: list(range(variable.n_categories))}), 0.0
    return Evidence({variable: [spn.assignment]}), 0.0
