from typing import Tuple, Optional, cast, List
import math
from spn.actions.map_algorithms.map_scenario import MapScenario
from spn.node.base import SPN
from spn.node.indicator import Indicator
from spn.node.sum import SumNode
from spn.utils.evidence import Evidence
from spn.structs import Variable


def do_max_product(map_scenario: MapScenario) -> Evidence:
    """Caller for max_product to adequate returned value to be only
    the Evidence"""
    return max_product(
        map_scenario.spn,
        map_scenario.query,
        map_scenario.evidence,
        map_scenario.marginalized,
    )[0]


def max_product(
    spn: SPN,
    query: List[Variable],
    evidence: Optional[Evidence],
    marginalized: Optional[List[Variable]],
) -> Tuple[Evidence, float]:
    """Max-product algorithm based on 'Sum-product networks: A new deep architecture
    Inference in Sum-Product Networks'"""
    if spn.type == "leaf":
        spn = cast(Indicator, spn)
        variable = spn.variable
        if evidence is not None:
            if variable in evidence:
                return evidence, spn.log_value(evidence)
            if marginalized is not None and variable in marginalized:
                return Evidence({variable: list(range(variable.n_categories))}), 0.0
        return Evidence({variable: [spn.assignment]}), 0.0
    child_map_results = [
        max_product(child, query, evidence, marginalized) for child in spn.children
    ]
    if spn.type == "sum":
        spn = cast(SumNode, spn)
        results = [
            (map_result[0], log_weight + map_result[1])
            for map_result, log_weight in zip(child_map_results, spn.log_weights)
        ]
        return max(results, key=lambda x: x[1])
    # Product
    new_evidence = Evidence()
    value = 0.0
    for result in child_map_results:
        new_evidence.merge(result[0])
        value += result[1]
    return new_evidence, value
