from typing import Callable, cast, Optional, Tuple, List, Dict
from functools import reduce
from spn.node.base import SPN
from spn.node.indicator import Indicator
from spn.node.sum import SumNode
from spn.node.product import ProductNode
from spn.utils.evidence import Evidence
from spn.structs import Variable
from spn.actions.map_algorithms.anytime_info import AnytimeInfo


# helper functions
def argmax(vector: List[float]) -> int:
    " Returns the position of the maximum value "
    i = 0
    for j in range(len(vector)):
        if vector[i] < vector[j]:
            i = j
    return i


def product(vector: List[int]) -> float:
    " Computes the product of the numbers in list vector"
    return reduce(lambda x, y: x * y, vector)


# Message passing algorithm
def lbp(
    spn: SPN,
    evidence: Evidence,
    marginalized: Optional[List[Variable]] = None,
    num_iterations: int = 30,
    tolerance: float = 1e-6,
) -> AnytimeInfo:
    " Implements Liu & Ihler 2011's max-sum-product belief propagation algorithm "
    sc = sorted(spn.scope())
    if marginalized is None:
        marginalized = []
    anytime_info = AnytimeInfo()
    # print(
    #    f"{len(sc)} variables: {len(sc)-len(evidence)-len(marginalized)} query, {len(evidence)} evidence, {len(marginalized)} marginalized."
    # )
    pi: Dict[Tuple[SPN, SPN], float] = {}  # downstream messages
    pi_i: Dict[
        Tuple[int, Indicator], List[float]
    ] = {}  # downstream messages from root (max) node
    ll: Dict[Tuple[SPN, SPN], float] = {}  # upstream messages
    ll_i: Dict[
        Tuple[SPN, int], List[float]
    ] = {}  # upstream messages to root (max) nodes
    parent: Dict[SPN, List[SPN]] = {}  # parent of each node
    parent_i: Dict[int, List[SPN]] = {}  # parent of root nodes
    bel: Dict[int, List[float]] = {}  # beliefs for root nodes
    num_sum, num_prod, num_leaves = 0, 0, 0
    prev_value = 0.0
    ### Initialization ################################################################
    for var in sc:
        bel[var.id] = [1 / var.n_categories for _ in range(var.n_categories)]
    for node in reversed(spn.topological_order()):
        assert len(node.children) <= 2
        if node.type == "leaf":
            num_leaves += 1
            node = cast(Indicator, node)
            ncat = node.variable.n_categories
            ll_i[(node, node.variable.id)] = [1.0 / ncat for _ in range(ncat)]
            if (node.variable.id, node) not in pi_i:
                pi_i[(node.variable.id, node)] = [1.0 / ncat for _ in range(ncat)]
            if node.variable.id in parent_i:
                parent_i[node.variable.id].append(node)
            else:
                parent_i[node.variable.id] = [node]
        else:
            if node.type == "sum":
                # node = cast(SumNode,node)
                num_sum += 1
            elif node.type == "product":
                # node = cast(ProductNode,node)
                num_prod += 1
            else:
                raise Exception("Unknown node type")
            for child in node.children:
                ll[(node, child)] = 0.5
                pi[(child, node)] = 0.5
                if child in parent:
                    parent[child].append(node)
                else:
                    parent[child] = [node]
    # ensure there's one leaf for each value of each variable
    # print(f"SPN has {num_sum} sums, {num_prod} products and {num_leaves} leaves.\n")
    assert num_sum + num_prod + num_leaves == len(spn.topological_order())
    assert num_leaves == sum(v.n_categories for v in spn.scope())
    # print("iteration | variational bound ")
    for iteration in range(num_iterations):
        ### Downward propagation ##########################################################
        # compute message from root node v to child (indicator) node
        for (var_id, node), values in pi_i.items():
            if sc[var_id] in evidence:
                # print(sc[v])
                ve = evidence[sc[var_id]][0]
                for j in range(sc[var_id].n_categories):
                    values[j] == 0.0
                values[ve] = 1.0
            else:
                Z = 0.0
                for j in range(node.variable.n_categories):
                    values[j] = product(
                        ll_i[(n, var_id)][j] for n in parent_i[var_id] if n != node
                    )
                    Z += values[j]
                # normalize messages (seems to be important only for marginalized variables)
                for j in range(node.variable.n_categories):
                    values[j] /= Z
            # print("root",v,node.assignment,values,Z)
        # compute remaining messages
        for node in reversed(spn.topological_order()):
            # compute messages pi_X^Y -- assumes incoming messages are normalized
            if node.type == "leaf":
                node = cast(Indicator, node)
                vid = node.variable.id
                maxbel = max(bel[vid])
                for pa in parent[node]:
                    if node.variable in marginalized:

                        pi1 = pi_i[(vid, node)][node.assignment] * product(
                            ll[(opa, node)] for opa in parent[node] if opa != pa
                        )
                        pi0 = (1 - pi_i[(vid, node)][node.assignment]) * product(
                            1.0 - ll[(opa, node)] for opa in parent[node] if opa != pa
                        )
                    else:
                        # TODO: handle evidence more efficiently
                        if bel[vid][node.assignment] == maxbel:
                            pi1 = pi_i[(vid, node)][node.assignment] * product(
                                ll[(opa, node)] for opa in parent[node] if opa != pa
                            )
                        else:
                            pi1 = 0.0
                        pi0 = 0.0
                        for j in range(node.variable.n_categories):
                            if j != node.assignment:
                                if bel[vid][j] == maxbel:
                                    pi0 += pi_i[(vid, node)][j]
                        pi0 *= product(
                            1.0 - ll[(opa, node)] for opa in parent[node] if opa != pa
                        )
                    if pi1 + pi0 > 0:
                        pi[(node, pa)] = pi1 / (pi1 + pi0)  # normalization
                    else:
                        # NUMERICAL ERROR: fallback to uniform for now (should fix this!)
                        pi[(node, pa)] = 0.5
                    # print("leaf", vid, node.assignment, pi[(node,pa)], pi1, pi0, pi_i[vid,node])
            elif node.type == "sum":
                if not node.root:
                    node = cast(SumNode, node)
                    assert len(parent[node]) == 1
                    pa = parent[node][0]
                    pi[(node, pa)] = (
                        node.weights[0] * pi[(node.children[0], node)]
                        + node.weights[1] * pi[(node.children[1], node)]
                    )
            else:  # Product
                if not (node.root):
                    assert len(parent[node]) == 1
                    pa = parent[node][0]
                    pi[(node, pa)] = (
                        pi[(node.children[0], node)] * pi[(node.children[1], node)]
                    )

        ### Upward propagation ##########################################################
        for node in spn.topological_order():
            # compute messages ll_X^U -- assumes incoming messages are normalized
            if node.type == "sum":
                node = cast(SumNode, node)
                ch1, ch2 = node.children
                if (
                    node.root
                ):  # root note receives indicator on value = 1 (to represent evidence on that node)
                    llp = 1.0
                else:
                    assert len(parent[node]) == 1
                    pa = parent[node][0]
                    llp = ll[(pa, node)]
                # left child
                ll1 = (1 - llp) * node.weights[1] * (1 - pi[(ch2, node)]) + llp * (
                    node.weights[0] * (1 - pi[(ch2, node)]) + pi[(ch2, node)]
                )
                ll0 = (1 - llp) * (
                    1 - pi[(ch2, node)] + node.weights[0] * pi[(ch2, node)]
                ) + llp * node.weights[1] * pi[(ch2, node)]
                ll[(node, ch1)] = ll1 / (ll0 + ll1)
                # right child
                ll1 = (1 - llp) * node.weights[0] * (1 - pi[(ch1, node)]) + llp * (
                    node.weights[1] * (1 - pi[(ch1, node)]) + pi[(ch1, node)]
                )
                ll0 = (1 - llp) * (
                    1 - pi[(ch1, node)] + node.weights[1] * pi[(ch1, node)]
                ) + llp * node.weights[0] * pi[(ch1, node)]
                ll[(node, ch2)] = ll1 / (ll0 + ll1)
            elif node.type == "product":
                ch1, ch2 = node.children
                if node.root:
                    llp = 1.0
                else:
                    assert len(parent[node]) == 1
                    pa = parent[node][0]
                    llp = ll[(pa, node)]
                # left child
                ll1 = (1.0 - llp) * (1.0 - pi[(ch2, node)]) + llp * pi[(ch2, node)]
                if ll1 + (1 - llp) > 0:
                    ll[(node, ch1)] = ll1 / (ll1 + 1 - llp)
                else:
                    ### NUMERICAL ERROR: FALLBACK TO UNIFORM (should fix this!)
                    ll[(node, ch2)] = 0.5
                # right child
                ll1 = (1.0 - llp) * (1.0 - pi[(ch1, node)]) + llp * pi[(ch1, node)]
                if ll1 + 1 - llp > 0:
                    ll[(node, ch2)] = ll1 / (ll1 + 1 - llp)
                else:  ### NUMERICAL ERROR: FALLBACK TO UNIFORM (should fix this!)
                    ll[(node, ch2)] = 0.5
        # compute messages to root nodes
        for (n, v) in ll_i.keys():
            llp1 = product(ll[(pa, n)] for pa in parent[n])
            llp0 = product(1.0 - ll[(pa, n)] for pa in parent[n])
            Z = llp1 + llp0 * (sc[v].n_categories - 1)
            for j in range(sc[v].n_categories):
                ll_i[(n, v)][j] = llp0 / Z
            ll_i[(n, v)][n.assignment] = llp1 / Z
        ### Compute beliefs ##########################################################
        for var in sc:
            if var in evidence:
                ve = evidence[var][0]
                for j in range(var.n_categories):
                    bel[var.id][j] == 0
                bel[var.id][ve] = 1.0
            else:
                Z = 0.0
                for j in range(var.n_categories):
                    b = product(ll_i[(n, var.id)][j] for n in parent_i[var.id])
                    Z += b
                    bel[var.id][j] = b
                if Z > 0:
                    for j in range(var.n_categories):
                        bel[var.id][j] /= Z
                else:  ## NUMERICAL ERROR: should fix this!
                    for j in range(var.n_categories):
                        bel[var.id][j] /= 1 / var.n_categories

        # print(bel)
        ev = Evidence(dict((var, [argmax(bel[var.id])]) for var in sc))
        anytime_info.new_lower_bound(spn.value(ev), ev)
        # print(x, spn.value(x))
        # extract approximated value (the marginal of the leaf)
        if spn.type == "sum":  # root node is sum
            spn = cast(SumNode, spn)
            value = (
                spn.weights[0] * pi[(spn.children[0], spn)]
                + spn.weights[1] * pi[(spn.children[1], spn)]
            )
            # print(f"{iteration:9} | {value}")
            if abs(value - prev_value) < tolerance:
                # early stop
                break
            prev_value = value

    ev = Evidence(dict((var, [argmax(bel[var.id])]) for var in sc))
    for var in marginalized:
        ev[var] = list(range(var.n_categories))
    anytime_info.new_lower_bound(spn.value(ev), ev)
    anytime_info.finish()
    return anytime_info
