from typing import List, Optional
import copy
from spn.utils.evidence import Evidence
from spn.structs import Variable
from spn.node.base import SPN


class MapScenario:
    def __init__(
        self,
        query: List[Variable],
        evidence: Optional[Evidence],
        marginalized: Optional[List[Variable]],
        spn: SPN,
    ):
        self.__query = query
        self.__evidence = evidence
        self.__marginalized = marginalized
        self.__spn = spn

    def __check_arguments(
        self,
        query: List[Variable],
        evidence: Optional[Evidence],
        marginalized: Optional[List[Variable]],
        spn: SPN,
    ):
        for var in query:
            if evidence is not None:
                assert var not in evidence
            if marginalized is not None:
                assert var not in marginalized
        if marginalized is not None and evidence is not None:
            for var in marginalized:
                assert var not in evidence
        for var in spn.scope():
            assert (
                var in query
                or (evidence is not None and var in evidence)
                or (marginalized is not None and var in marginalized)
            )

    @classmethod
    def only_query(cls, spn: SPN):
        return cls(spn.scope(), Evidence(), [], spn)

    @property
    def query(self) -> List[Variable]:
        return self.__query

    @property
    def evidence(self) -> Optional[Evidence]:
        return self.__evidence

    @evidence.setter
    def evidence(self, evidence: Evidence):
        self.__evidence = evidence

    @property
    def marginalized(self) -> Optional[List[Variable]]:
        return self.__marginalized

    @marginalized.setter
    def marginalized(self, marginalized: List[Variable]):
        self.__marginalized = marginalized

    @property
    def spn(self) -> SPN:
        return self.__spn

    def compatible(self, evidence: Evidence) -> bool:
        if self.__marginalized is not None:
            for variable in self.__marginalized:
                if evidence[variable] != list(range(variable.n_categories)):
                    return False
        if self.__evidence is not None:
            for var, values in self.__evidence.items():
                if evidence[var] != values:
                    return False
        for var in self.__query:
            if var not in evidence:
                return False
        for var in self.__spn.scope():
            if var not in evidence:
                return False
        return True


def map_scenarios_to_file(map_scenarios: List[MapScenario], filename: str):
    """Writes a list of map scenarios to a file"""
    with open(filename, "w") as the_file:
        for map_scenario in map_scenarios:
            the_file.write("q ")
            the_file.write(" ".join([str(var.id) for var in map_scenario.query]))
            the_file.write("\n")
            if map_scenario.marginalized is not None:
                the_file.write("m ")
                the_file.write(
                    " ".join([str(var.id) for var in map_scenario.marginalized])
                )
                the_file.write("\n")
            if map_scenario.evidence is not None:
                the_file.write("e ")
                the_file.write(
                    " ".join(
                        [
                            f"{var.id} {values[0]}"
                            for var, values in map_scenario.evidence.items()
                        ]
                    )
                )
                the_file.write("\n")
            the_file.write("-\n")


def map_scenarios_from_file(filename: str, spn: SPN) -> List[MapScenario]:
    """Reads a list of map scenarios from a file"""
    map_scenarios: List[MapScenario] = []
    variables = spn.scope()
    with open(filename, "r") as the_file:
        map_scenario = None
        for line in the_file.readlines():
            line = line.strip()
            if line.startswith("q"):
                if map_scenario is not None:
                    map_scenarios.append(map_scenario)
                    map_scenario = None
                query_vars = [variables[int(var_id)] for var_id in line.split(" ")[1:]]
                map_scenario = MapScenario(query_vars, None, None, spn)
            elif line.startswith("m"):
                if map_scenario is not None:
                    marginalized_vars = [
                        variables[int(var_id)] for var_id in line.split(" ")[1:]
                    ]
                    map_scenario.marginalized = marginalized_vars
            elif line.startswith("e"):
                if map_scenario is not None:
                    evidence = Evidence()
                    splitted_line = line.split(" ")[1:]
                    index = 0
                    while index < len(splitted_line):
                        variable = variables[int(splitted_line[index])]
                        values = [int(splitted_line[index + 1])]
                        evidence[variable] = values
                        index += 2
                    map_scenario.evidence = evidence
        if map_scenario is not None:
            map_scenarios.append(map_scenario)
    return map_scenarios
