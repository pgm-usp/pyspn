from typing import Tuple, List, Optional
import copy
from spn.node.base import SPN
from spn.utils.evidence import Evidence
from spn.actions.map_algorithms.anytime_info import AnytimeInfo
from spn.actions.map_algorithms.map_scenario import MapScenario
from spn.structs import Variable


def local_search(
    map_scenario: MapScenario, initial_evidence: Optional[Evidence] = None
) -> AnytimeInfo:
    evidence = map_scenario.evidence
    marginalized = map_scenario.marginalized
    spn = map_scenario.spn
    query = map_scenario.query
    if initial_evidence is not None:
        assert map_scenario.compatible(initial_evidence)
    else:
        if evidence is not None:
            initial_evidence = copy.deepcopy(evidence)
        else:
            initial_evidence = Evidence()
        if marginalized is not None:
            for variable in marginalized:
                initial_evidence[variable] = list(range(variable.n_categories))
        for variable in query:
            initial_evidence[variable] = [0]
    return local_search_main(spn, initial_evidence, query)


def local_search_main(
    spn: SPN, best_evidence: Evidence, query: List[Variable]
) -> AnytimeInfo:
    best_value = spn.log_value(best_evidence)
    anytime_info = AnytimeInfo()
    anytime_info.new_lower_bound(best_value, best_evidence)
    changed = True
    while changed:
        changed = False
        for variable in query:
            for value in range(variable.n_categories):
                new_evidence = copy.deepcopy(best_evidence)
                new_evidence[variable] = [value]
                new_value = spn.log_value(new_evidence)
                if new_value > best_value:
                    changed = True
                    best_value = new_value
                    best_evidence = new_evidence
                    anytime_info.new_lower_bound(best_value, best_evidence)
    anytime_info.new_lower_bound(best_value, best_evidence)
    anytime_info.finish()
    return anytime_info
