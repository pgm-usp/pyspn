"""SPN actions: Loading all modules to populate action namespace and allow matching
on config/spn.json"""

import spn.actions.bag
import spn.actions.copy
import spn.actions.classify
import spn.actions.em
import spn.actions.graph
import spn.actions.io
import spn.actions.learn
import spn.actions.likelihood
import spn.actions.map
import spn.actions.mis
import spn.actions.partition
import spn.actions.report
