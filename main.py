"""Main: Test file to run the app"""
from app import App


def main():
    """Helper function to profile the main app"""
    App().start()


if __name__ == "__main__":
    main()
